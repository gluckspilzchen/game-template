enum Coord { Point (x : Int, y : Int); }
enum Neighbourhood {
	Moore;
	VonNeumann;
}

class Level extends dn.Process {
	public var TILES(default, null) = new Array();
	public var TEMP_TILES(default, null) : Map<Coord, ProtoEntity> = new Map();
	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;

	public var wid : Int;
	public var hei : Int;

	public var level : World_Level;
	var painter : LevelPainter;
	var tilesetSource : h2d.Tile;
	var normalsSource : h2d.Tile;
	var maskSource : h2d.Tile;
	var valueSource : h2d.Tile;

	var invalidated = true;

	public var collisions(default, null) : Map<Coord, Bool> = new Map();

	var texts : h2d.Object;
	var fgTexts : h2d.Object;
	var bg : h2d.TileGroup;
	var bottom : h2d.TileGroup;
	var bottomFx : h2d.Object;
	var dyns : h2d.Object;
	var top : h2d.TileGroup;
	var topFx : h2d.Object;
	var post : h2d.Object;

	public function new(l : World_Level) {
		super(Game.ME);
		createRootInLayers(Game.ME.scroller, Const.DP_BG);

		level = l;

		tilesetSource = hxd.Res.world.tileset.toAseprite().toTile();
		normalsSource = hxd.Res.world.normals.toAseprite().toTile();
		maskSource = hxd.Res.world.idmask.toAseprite().toTile();
		valueSource = hxd.Res.world.value.toAseprite().toTile();

		wid = l.l_Entities.cWid;
		hei = l.l_Entities.cHei;

		texts = new h2d.Object();
		game.scroller.add(texts, Const.DP_BG);

		fgTexts = new h2d.Object();
		game.scroller.add(fgTexts, Const.DP_TOP);

		dyns = new h2d.Object(root);
		dyns.y = LevelPainter.tileSize*1/4;
		bg = new h2d.TileGroup(tilesetSource);
		game.root.add(bg, Const.DP_TRUE_BG);
		bottom = new h2d.TileGroup(tilesetSource);
		bottom.x = LevelPainter.tileSize/2;
		bottom.y = LevelPainter.tileSize*3/4;
		game.scroller.add(bottom, Const.DP_BG);
		bottomFx = new h2d.Object();
		bottomFx.y = LevelPainter.tileSize/2;
		game.scroller.add(bottomFx, Const.DP_FX_BG);
		top = new h2d.TileGroup(tilesetSource);
		top.x = LevelPainter.tileSize/2;
		top.y = LevelPainter.tileSize/4;
		game.scroller.add(top, Const.DP_FRONT);
		topFx = new h2d.Object();
		game.scroller.add(topFx, Const.DP_FX_FRONT);
		post = new h2d.Object(Boot.ME.postRoot);
	}
	
	override function onDispose() {
		super.onDispose();

		texts.remove();
		fgTexts.remove();

		dyns.remove();
		top.remove();
		topFx.remove();
		bg.remove();
		bottom.remove();
		bottomFx.remove();
		post.remove();

		for (t in TILES) {
			t.destroy();
		}
		for (t in TEMP_TILES.iterator()) {
			t.destroy();
		}
		TILES = null;
		TEMP_TILES = null;
	}

	public function initLevel() {
		painter = new LevelPainter(this, Game.ME.curLevelIdx);
	}

	public inline function isValidX(cx) return (cx>=0 && cx<wid);
	public inline function isValidY(cy) return (cy>=0 && cy<hei);
	public inline function isValid(cx,cy) return isValidX(cx) && isValidY(cy);
	public inline function coordId(cx,cy) return cx + cy*wid;


	public function render() {
		top.clear();
		topFx.removeChildren();
		bg.clear();
		bottom.clear();
		bottomFx.removeChildren();
		texts.removeChildren();
		fgTexts.removeChildren();
		painter.runWFC();
		dyns.removeChildren();
		post.removeChildren();

		var size = LevelPainter.tileSize;
		for (i in 0...painter.bgW) {
			for (j in 0...painter.bgH) {
				var at = painter.bgLayer[i + j * (painter.bgW)];
				var x = at.x * size;
				var y = at.y * size;
				var t = tilesetSource.sub(x, y, size, size);
				bg.add(i*size, j*size, t);
			}
		}
		if (Settings.animatedEffects) {
			var shader = new vfx.shdr.DrawNormals(normalsSource);
			shader.setPriority(50);
			bg.addShader(shader);
			var shader = new vfx.shdr.DrawValueTexture(maskSource, valueSource);
			shader.setPriority(50);
			bg.addShader(shader);
		}
		for (l in painter.bottomLayers) {
			for (i in 0...wid-1) {
				for (j in 0...hei-1) {
					var at = l[i + j * (wid - 1)];
					var x = at.x * size;
					var y = at.y * size;
					var t = tilesetSource.sub(x, y, size, size);
					bottom.add(i*size, j*size, t);
				}
			}
		}
		if (Settings.animatedEffects) {
			var shader = new vfx.shdr.DrawNormals(normalsSource);
			shader.setPriority(50);
			bottom.addShader(shader);
			var shader = new vfx.shdr.DrawValueTexture(maskSource, valueSource);
			shader.setPriority(50);
			bottom.addShader(shader);
		}

		for (l in painter.topLayers) {
			for (i in 0...wid-1) {
				for (j in 0...hei-1) {
					var at = l[i + j * (wid - 1)];
					var x = at.x * size;
					var y = at.y * size;
					var t = tilesetSource.sub(x, y, size, size);
					top.add(i*size, j*size, t);
				}
			}
		}

		if (Settings.animatedEffects) {
			var shader = new vfx.shdr.DrawNormals(normalsSource);
			shader.setPriority(50);
			top.addShader(shader);
			var shader = new vfx.shdr.DrawValueTexture(maskSource, valueSource);
			shader.setPriority(50);
			top.addShader(shader);
		}
	}

	public function hasCollision(cx, cy) : Bool {
		return collisions.exists(Point(cx, cy));
	}

	public function shouldChangeLevel(cx, cy) : Bool {
		return (cx == 0 || cx == (wid - 1) || cy == 0 || cy == (hei - 1)) && !hasCollision(cx, cy);
	}

	override function update() {
		super.update();
	}

	override function postUpdate() {
		super.postUpdate();

		if( invalidated ) {
			invalidated = false;
			render();
		}
	}

	public function addCollision(cx, cy) {
		collisions.set(Point(cx, cy), true);
	}

	public function removeCollision(cx, cy) {
		collisions.remove(Point(cx, cy));
	}

	public function checkNeighbours(n : Neighbourhood, f : (cx : Int, cy : Int) -> Bool, cx : Int, cy : Int) {
		var res = switch n {
			case Moore: f(cx+1, cy+1) || f(cx-1, cy-1) || f(cx-1, cy+1) || f(cx+1, cy-1);
			case VonNeumann: false;
		};
		return res || f(cx+1, cy) || f(cx-1, cy) || f(cx, cy+1) || f(cx, cy-1);
	}

	override function onResize() {
		super.onResize();

		invalidated = true;
	}
}

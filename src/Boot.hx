class Boot extends hxd.App {
	public static var ME : Boot;

	private var objectScene : h2d.Scene;
	public var colorTarget : h3d.mat.Texture;
	public var darkTarget : h3d.mat.Texture;
	public var normalsTarget : h3d.mat.Texture;
	public var valueTextureTarget : h3d.mat.Texture;
	public var renderTarget : h3d.mat.Texture;

	private var bg : h2d.Bitmap;

	private var postScene : h2d.Scene;
	private var post : h2d.Bitmap;
	public var postTarget : h3d.mat.Texture;
	public var postRoot : h2d.Object;

	private var uiScene(get, never) : h2d.Scene; inline function get_uiScene() return s2d;
	public var uiRoot : h2d.Object;

	// Boot
	static function main() {
		new Boot();
	}

	// Engine ready
	override function init() {
		ME = this;

		objectScene = new h2d.Scene();
		new tools.RenderContext.ObjectRenderContext(objectScene);

		colorTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		darkTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		normalsTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		valueTextureTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		renderTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);

		postScene = new h2d.Scene();
		new tools.RenderContext.BaseRenderContext(postScene);
		bg = new h2d.Bitmap(h2d.Tile.fromTexture(renderTarget));
		postScene.add(bg, 1);

		postRoot = new h2d.Object();
		postScene.add(postRoot, 2);

		postTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		post = new h2d.Bitmap(h2d.Tile.fromTexture(postTarget));
		uiScene.add(post, 1);

		uiRoot = new h2d.Object();
		uiScene.add(uiRoot, 2);

		onResize();

		new Main(objectScene, uiScene);
	}

	override function onResize() {
		super.onResize();
		dn.Process.resizeAll();

		post.setScale(Const.SCALE);

		objectScene.checkResize();
		postScene.checkResize();
		uiScene.checkResize();

		colorTarget.resize(engine.width, engine.height);
		darkTarget.resize(engine.width, engine.height);
		normalsTarget.resize(engine.width, engine.height);
		valueTextureTarget.resize(engine.width, engine.height);
		renderTarget.resize(engine.width, engine.height);
		postTarget.resize(engine.width, engine.height);

		bg.tile.scaleToSize(engine.width, engine.height);
		post.tile.scaleToSize(engine.width, engine.height);
	}

	override function dispose() {
		super.dispose();

		if (postScene != null)
			postScene.dispose();

		if (uiScene != null)
			uiScene.dispose();
	}

	var speed = 1.0;
	override function update(deltaTime:Float) {
		super.update(deltaTime);

		var tmod = hxd.Timer.tmod * speed;
		dn.Process.updateAll(tmod);
	}

	override function render (e:h3d.Engine) {

		engine.pushTargets([colorTarget, darkTarget, normalsTarget, valueTextureTarget]);

		engine.clear(0, 1);
		objectScene.render(e);
		engine.popTarget();

		engine.pushTarget(renderTarget);
		engine.clear(0, 1);
		if (Game.ME != null)
			Game.ME.lights.render(e);
		engine.popTarget();

		engine.pushTarget(postTarget);
		engine.clear(0, 1);
		postScene.render(e);
		engine.popTarget();

		uiScene.render(e);
	}
}


package vfx.shdr;

class DrawValueTexture extends hxsl.Shader {
	static var colors = [ for (i in [0xaf7f66, 0x617041, 0x1f1427]) h3d.Vector4.fromColor(i) ];
    static var SRC = {
		@param var maskColors : Array<Vec4, 3>;
		@param var mask : Sampler2D;
		@param var valueTexture : Sampler2D;

		@var var calculatedUV : Vec2;
		@global var grid : Int;

		@input var input : {
			var position : Vec2;
			var uv : Vec2;
			var color : Vec4;
		};

		var pixelValueTexture : Vec4;

		function isSameColor(c1 : Vec4, c2 : Vec4) : Bool {
			return (c1.x == c2.x && c1.y == c2.y && c1.z == c2.z);
		}

		function fragment() {
			var id = mask.get(calculatedUV);
			var offset = 2.;
			if (isSameColor(id, maskColors[2])) {
				offset = 1.;
			}

			var tileUV = mod(input.position, vec2(grid))/grid;
			var tsize = valueTexture.size();

			var offsetX = mod(offset, tsize.x);
			var offsetY = offset / tsize.x;

			var coordX = (offsetX + tileUV.x) * grid / tsize.x;
			var coordY = (offsetY + tileUV.y) * grid / tsize.y;

			pixelValueTexture = valueTexture.get(vec2(coordX, coordY));
		}
    }


	public function new(m : h2d.Tile, vt : h2d.Tile) {
		super();

		maskColors = colors;

		mask = m.getTexture();
		mask.filter = Nearest;

		valueTexture = vt.getTexture();
		valueTexture.filter = Nearest;
	}
}

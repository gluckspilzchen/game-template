package vfx.shdr;

class Init extends hxsl.Shader {

	static var SRC = {
		@global var vertical : Vec4;

		var pixelNormal : Vec4;
		var pixelDark : Vec4;
		var pixelValueTexture : Vec4;

		function __init__() {
			pixelNormal = vertical;
			pixelDark = vec4(0, 0, 0, 0);
			pixelValueTexture = vec4(-1, -1, -1, 1);
		}
	};
}

class Output extends hxsl.Shader {

	static var SRC = {

		var output : {
			var position : Vec4;
			var color : Vec4;
			var dark : Vec4;
			var normal : Vec4;
			var valueTexture : Vec4;
		}

		var pixelColor : Vec4;
		var pixelDark : Vec4;
		var pixelNormal : Vec4;
		var pixelValueTexture : Vec4;

		function fragment() {
			if( pixelColor.a > 0.001 ) {
				output.normal = pixelNormal;
				output.dark = pixelDark;
				output.valueTexture = pixelValueTexture;
			}
		}
	};
}

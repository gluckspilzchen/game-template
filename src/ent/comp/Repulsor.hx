package ent.comp;

class Repulsor extends EntityComponent {
	var mass : Int;
	var collidable : Collidable;

	public function new (c : Collidable, m) {
		super(c.me);
		this.mass = m;
		this.collidable = c;

		systems.Kinetic.ME.addRepulsor(c, m);
	}

	override function dispose() {
		super.dispose();

		systems.Kinetic.ME.removeRepulsor(collidable);
	}
}

package ent.comp;
import haxe.ds.Option;

enum CollisionKind {
	Solid;
	Soft;
}

class ColPoint {
	public var p(default, null) : tools.CPoint;
	public var radius(default, null) : Float;
	public var canCollide : () -> Bool;

	public function new(p : tools.CPoint, r : Float, f : () -> Bool) {
		this.p = p;
		this.radius = r;
		canCollide = f;
	}
}

class Collidable extends EntityComponent {
    public static var ALL : Array<Collidable> = [];

	public var kind : CollisionKind;
	private var isKilling : Bool;
	public var canCollide : Bool;

	public function new (me : Entity, k : CollisionKind, isKilling : Bool, canCollide : Bool) {
		super(me);

		this.canCollide = canCollide;

		kind = k;
		this.isKilling = isKilling;

		systems.Kinetic.ME.addCollider(this);

		me.getCollidable = () -> Some (this);

		ALL.push(this);
	}

	public function willCollide(ignored : Array<Collidable>) {
		var collided = ALL.filter(function (ec) { return !ignored.contains(ec) && checkHit(ec.me); });
		return collided.length > 0;
	}

	public function collide(collided : Array<Collidable>) {
		if (!me.isDead && isKilling && me.checkHit(game.hero, false)) {
			game.hero.makeCollideDead(me);
		}
		onCollide(collided);
	}

	dynamic public function onCollide(e : Array<Collidable>) {
	}

	dynamic public function onCheck() {
	}

	public function checkHit(e : Entity, isEffect = false) {
		return !e.isDead && !me.isDead && e != me && me.checkHit(e, isEffect);
	}

	public function isHero() {
		return me == game.hero;
	}

	override function dispose() {
		super.dispose();

		systems.Kinetic.ME.removeCollider(this);

		ALL.remove(this);
	}

	public function computeCollision(ignored : Array<Collidable>) {
		var e = me;
		var x = e.centerX;
		var y = e.centerY;
		var dx = e.dxTotal;
		var dy = e.dyTotal;
		var r = e.collisionRadius;

		var isCandidate = function (cp : ColPoint) {
			var tx = (cp.p.centerX - x);
			var ty = (cp.p.centerY - y);
			var dot = dx * tx + dy * ty;

			var norm = Math.sqrt ((tx * tx + ty * ty) * (dx * dx + dy * dy));

			return
				dot > 0.9 * norm &&
				(tools.Geometry.hasCircleLineIntersection(x, y, dx, dy, cp.p.centerX, cp.p.centerY, cp.radius + r));
		}

		var collidableCandidates = ALL.filter(function (ec) {
			return
				ec.me != e &&
				(ignored == null ||	!(ignored.contains(ec)));
		});
		var allCandidates = collidableCandidates.map((e) -> new ColPoint(new tools.CPoint(e.cx, e.cy, e.xr, e.yr), e.me.collisionRadius, () -> e.canCollide));

		for (p in Game.ME.level.collisions.keys()) {
			switch (p) {
				case Point (x, y): allCandidates.push(new ColPoint(new tools.CPoint(x, y, 0.5, 0.5), 0, () -> true));
			}
		}

		var candidates = allCandidates.filter(isCandidate);

		if (candidates.length == 0)
			return None;
		else {
			var dh = new dn.DecisionHelper(candidates);
			dh.score( (ec)->-ec.p.distPx(e) + ec.radius );
			var b = dh.getBest();
			var p = tools.Geometry.getCircleLineIntersection(x, y, dx, dy, b.p.centerX, b.p.centerY, b.radius + r);
			switch p {
				case Some (p):
					return Some ({ point : p, canCollide : b.canCollide });
				case None:
					return None;
			};
		}
	}
}

0 => global int seed;

Gain masterVolume;
global Gain musicVolume;
global Gain foleyVolume;
global Gain buttonVolume;
0.1 => buttonVolume.gain;
global Dyno lim;
0::ms => lim.attackTime;
musicVolume => lim => masterVolume => dac;
foleyVolume => lim;
buttonVolume => lim;

global Conductor conductor;
global Event intro;
fun void introControl() {
	intro => now;
	while(true) {
		conductor.start();
		intro => now;
		conductor.exit();
	}
}
spork ~ introControl();

global Event menu;
fun void menuControl() {
	while(true) {
		menu => now;
		conductor.menu();
	}
}
spork ~ menuControl();

global Event game;
fun void gameControl() {
	while(true) {
		game => now;
		conductor.unsilence();
	}
}
spork ~ gameControl();

0.5 => global float volume;
0.5 => global float mVolume;
0.5 => global float fVolume;
global Event volumeChange;

fun void volumeControl() {
	while(true) {
		volume => masterVolume.gain;
		mVolume => musicVolume.gain;
		fVolume => foleyVolume.gain;
		volumeChange => now;
	}
}

Machine.add("foley.ck");
spork ~ volumeControl();

while(true) {
	1::second => now;
}
